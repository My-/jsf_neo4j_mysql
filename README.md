# This is year 3 project for Advance Data Centric Web Application module.


Extras:
- Changed color of the table
- Inside table navigations links replased with buttons(much clearear now)
- Java Docs
- Added extra validation on input field:
  - Add cours duration can't be shorter then 1 year.
  - validated input length.
- Showing student address in the tables(was not required)
- w3.css styles
- I would like to believe: good coding practice and readable code.
