package com.student.DAOs;

import com.student.Models.StudentModel;
import org.neo4j.driver.v1.*;

import static org.neo4j.driver.v1.Values.parameters;

public class Neo4jDAO {
    private static final String user = "neo4j";
    private static final String pass = "neo4jdb";

    private Neo4jDAO(){}

    /**
     * Adds student to Neo4j DB.
     * @param student we want to add to DB.
     */
    public static void addStudent(StudentModel student){
        Driver driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic(user, pass));
        Session session = driver.session();

        session.writeTransaction(tx ->
            tx.run("CREATE(:STUDENT{name: $name, address: $address})",
                    parameters("name", student.getName(), "address", student.getAddress()))
        );

        session.close();
        driver.close();
    }

    /**
     * Delete student from DB.
     * @param student we want to delete.
     */
    public static void deleteStudent(StudentModel student){
        Driver driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic(user, pass));
        Session session = driver.session();

        session.run("MATCH(s:STUDENT{name: $name}) delete s;",
                parameters("name",student.getName()) );

        session.close();
        driver.close();
    }

}
