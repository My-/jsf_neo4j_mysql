package com.student.DAOs;

import com.student.Models.CourseModel;
import com.student.Models.StudentModel;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlDAO {
    private DataSource mysqlDS;
    
    // mysql check port - show global variables like '%port%';

    public MySqlDAO() throws NamingException, SQLException {
        System.out.println("MySQL DAO..");
        Context context = new InitialContext();
        String jndiName = "java:comp/env/studentDB"; // name in web.xml & context.xml
        mysqlDS = (DataSource) context.lookup(jndiName);
    }

    /**
     * Get all courses from DB.
     * @return Courses as a list
     * @throws SQLException
     */
    public List<CourseModel> getCourses() throws SQLException {
        System.out.println("MySqlDAO.getCourses()");
        List<CourseModel> courses = new ArrayList<>();
        Connection conn = mysqlDS.getConnection();
        PreparedStatement myStmt = conn.prepareStatement(
                "select * from course;"
        );
        ResultSet rs = myStmt.executeQuery();

        while( rs.next() ) {
            courses.add(new CourseModel(
                    rs.getString("cID"),
                    rs.getString("cName"),
                    rs.getInt("duration")
            ));
        }

        conn.close();
        return courses;
    }

    /**
     * Add course to DB.
     * @param course to be added to DB.
     * @throws SQLIntegrityConstraintViolationException if course exist.
     * @throws SQLException
     */
    public void addCourse(CourseModel course) throws SQLException {
    	Connection conn = mysqlDS.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO course VALUES(?, ?, ?);"
        );
        statement.setString(1, course.getId());
        statement.setString(2, course.getName());
        statement.setInt(3, course.getDuration());

        statement.executeUpdate();
        conn.close();
    }

    /**
     * Get all students who attending given course.
     * @param course we inteested in
     * @return List of studens attending given course.
     * @throws SQLException
     */
    public List<StudentModel> getStudents(CourseModel course) throws SQLException {
        System.out.println("MySqlDAO.getStudents(CourseModel course)");
        List<StudentModel> students = new ArrayList<>();
        Connection conn = mysqlDS.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "SELECT * FROM student WHERE cID=?;"
        );
        statement.setString(1, course.getId());
        ResultSet rs = statement.executeQuery();

        while( rs.next() ) {
            students.add(new StudentModel(
                    rs.getString("sid"),
                    rs.getString("name"),
                    rs.getString("address"),
                    course
            ));
        }

        conn.close();
        return students;
    }

    /**
     * Deletes given course.
     * @param course to be deleted.
     * @throws SQLException
     */
    public void deleteCourse(CourseModel course) throws SQLException {
    	Connection conn = mysqlDS.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "DELETE FROM course WHERE cID=?;"
        );
        statement.setString(1, course.getId());
        statement.executeUpdate();
        conn.close();
    }


    ////////////////////////////// Managed Students ////////////////////////////////////

    /**
     * Get all students from DB.
     * @return All students in DB.
     * @throws SQLException
     */
    public List<StudentModel> getStudents() throws SQLException {
        System.out.println("MySqlDAO.getStudents()");
        List<StudentModel> students = new ArrayList<>();
        Connection conn = mysqlDS.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "SELECT * FROM student JOIN course c2 on student.cID = c2.cID;"
        );
        ResultSet rs = statement.executeQuery();

        while( rs.next() ) {
            students.add(new StudentModel(
                    rs.getString("sid"),
                    rs.getString("name"),
                    rs.getString("address"),
                    new CourseModel(
                            rs.getString("cID"),
                            rs.getString("cName"),
                            rs.getInt("duration")
                    )
            ));
        }

        conn.close();
        return students;
    }

    /**
     * Add student to DB.
     * @param student we wish to add to DB.
     * @return lines changed.
     * @throws SQLIntegrityConstraintViolationException if student exist.
     * @throws SQLException
     */
    public void addStudent(StudentModel student) throws SQLException {
        System.out.println("MySqlDAO.addStudent(StudentModel student)");
        Connection conn = mysqlDS.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO student VALUES(?, ?, ?, ?);"
        );
        statement.setString(1, student.getId());
        statement.setString(2, student.getCourse().getId());
        statement.setString(3, student.getName());
        statement.setString(4, student.getAddress());

        statement.executeUpdate();
        conn.close();
    }

    /**
     * Deletes given course.
     * @param student of the student to be deleted.
     * @throws SQLException
     */
    public void deleteStudent(StudentModel student) throws SQLException {
        System.out.println("MySqlDAO.deleteStudent(StudentModel student) : "+ student.getId());
        Connection conn = mysqlDS.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "DELETE FROM student WHERE sid=?;"
        );
        statement.setString(1, student.getId());
        statement.executeUpdate();
        conn.close();
    }
}
