package com.student.DAOs;

/**
 * Error codes.
 */
public final class Codes {
//        Duplicate entry 'G00123456' for key 'PRIMARY'
//        1062
//        Data truncation: Data too long for column 'cID' at row 1
//        1406
//        Cannot add or update a child row: a foreign key constraint fails (`studentDB`.`student`, CONSTRAINT `student_ibfk_1` FOREIGN KEY (`cID`) REFERENCES `course` (`cID`))
//        1452
    public static final int DUPLICATE_ENTRY          = 1062;
    public static final int DATA_TRUNCATION          = 1406;
    public static final int FOREIGN_KEY_CONSTRAINT   = 1452;
}
