package com.student.Controllers;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import com.student.DAOs.MySqlDAO;
import com.student.Models.CourseModel;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;

import java.sql.SQLException;
import java.util.*;

@ManagedBean
@SessionScoped
public class CourseController {
    private MySqlDAO dao;
    private List<CourseModel> courses;

    /**
     * Constructor for creating Course controller.
     */
    public CourseController(){
        System.out.println("CourseController...");
        try {
            this.dao = new MySqlDAO();
        }
        catch (NamingException | SQLException e) {
        	e.printStackTrace();
        }
    }

    /**
     * Loads courses from DB and saves them to this.courses list.
     * @return list of courses in DB.
     */
    public List<CourseModel> loadCourses() {
        System.out.println("CourseController.loadCourses()");
        
        try {
			this.courses = this.dao.getCourses();
		}
        catch (SQLException | NullPointerException e) {
			FacesMessage message =
                    new FacesMessage("Error: Cannot connect to MySQL Database");
            FacesContext.getCurrentInstance().addMessage(null, message);
		}
        
        return this.courses;
    }

    /**
     * Getter for courses list.
     * @return list of courses.
     */
    public List<CourseModel> getCourses() {
        System.out.println("CourseController.getCourses()");
        return courses;
    }

    /**
     * Add course to MySQL DB.
     * @param course course we want to add to DB.
     * @return page name we go after we done.
     */
    public String addCourse(CourseModel course){
        System.out.println("CourseController.addCourse(CourseModel course)");
        try {
            this.dao.addCourse(course);
        }
        catch (MySQLIntegrityConstraintViolationException e) {
        	FacesMessage message = 
        			new FacesMessage("Error: Course ID "+ course.getId().toUpperCase() +" already exist!");
        	FacesContext.getCurrentInstance().addMessage(null, message);
        }
        catch (SQLException | NullPointerException e) {
			FacesMessage message =
                    new FacesMessage("Error: Cannot connect to MySQL Database");
            FacesContext.getCurrentInstance().addMessage(null, message);
		}

        return AppPages.COURSES;
    }

    /**
     * Delete course from MySQL DB.
     * @param course we want to delete.
     * @return page name we go after we done.
     */
    public String deleteCourse(CourseModel course){
        System.out.println("CourseController.deleteCourse(CourseModel course)");
        try {
            this.dao.deleteCourse(course);
        }
        catch (MySQLIntegrityConstraintViolationException e) {
            FacesMessage message =
                    new FacesMessage("Error: Can't delete course: "+ course.getId().toUpperCase() +" as there are associated Students!");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        catch (SQLException | NullPointerException e) {
            FacesMessage message =
                    new FacesMessage("Error: Cannot connect to MySQL Database");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

        return AppPages.COURSES;
    }
}

