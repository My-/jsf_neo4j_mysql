package com.student.Controllers;

/**
 * Class holds all pages as constants.
 */
public final class AppPages {
    public static final String ADD_COURSE = "add_course";
    public static final String ADD_STUDENT = "add_student";
    public static final String COURSE_STUDENTS = "courses_students";
    public static final String STUDENT_FULL_DETAILS = "full_details_student";
    public static final String HOME = "index";
    public static final String COURSES = "list_courses";
    public static final String STUDENTS = "list_students";
}
