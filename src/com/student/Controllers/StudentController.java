package com.student.Controllers;

import com.mysql.jdbc.MysqlDataTruncation;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import com.student.DAOs.Codes;
import com.student.DAOs.MySqlDAO;
import com.student.DAOs.Neo4jDAO;
import com.student.Models.CourseModel;
import com.student.Models.StudentModel;

import org.neo4j.driver.v1.exceptions.ClientException;
import org.neo4j.driver.v1.exceptions.ServiceUnavailableException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@ManagedBean
@SessionScoped
public class StudentController {
    private List<StudentModel> students;
    private MySqlDAO dao;

    /**
     * Constructor for creating Student Controller.
     */
    public StudentController() {
        System.out.println("StudentController...");
        try {
            this.dao = new MySqlDAO();
        }
        catch (SQLException | NamingException e) {
        	e.printStackTrace();
        }
    }

    /**
     * Getter for this.students.
     * @return student list.
     */
    public List<StudentModel> getStudents() {
        return students;
    }

    /**
     * Load students from DB.
     * @return List of students.
     */
    public List<StudentModel> loadStudents() {
        try {
            this.students = this.dao.getStudents();
        }
        catch (SQLException | NullPointerException e) {
			FacesMessage message =
                    new FacesMessage("Error: Cannot connect to MySQL Database");
            FacesContext.getCurrentInstance().addMessage(null, message);
		}

        return this.students;
    }

    /**
     * Load students who attending given course.
     * @param course course we are interested.
     * @return list of students attending given course.
     */
    public List<StudentModel> loadStudents(CourseModel course) {
        System.out.println("StudentController.loadStudents(CourseModel course)");
        try {
            this.students = this.dao.getStudents(course);
        } 
        catch (SQLException | NullPointerException e) {
			FacesMessage message =
                    new FacesMessage("Error: Cannot connect to MySQL Database");
            FacesContext.getCurrentInstance().addMessage(null, message);
		}

        return this.students;
    }

    /**
     * Delegation method for load students
     * @param course course we are interested.
     * @return page name we go after we done.
     */
    public String showStudents(CourseModel course){
        System.out.println("StudentController.showStudents(CourseModel course)");
        this.loadStudents(course);

        return AppPages.COURSE_STUDENTS;
    }

    /**
     * Get single student full details. Picks from the student array and sets array to that single element.
     * @param student we are interested at.
     * @return page name we go after we done.
     */
    public String showStudent(StudentModel student){
        System.out.println("StudentController.showStudents(CourseModel course)");
        this.students = new ArrayList<>(1);
        this.students.add(student);

        return AppPages.STUDENT_FULL_DETAILS;
    }

    /**
     * Add student to DB (MySQL & Neo4j)
     * @param student we want to add to DB's.
     * @return page name we go after we done.
     */
    public String addStudent(StudentModel student){
        try {
            this.dao.addStudent(student);
        }
        catch (MySQLIntegrityConstraintViolationException | MysqlDataTruncation e) {
            FacesMessage message = new FacesMessage();

            switch (e.getErrorCode()){
                case Codes.FOREIGN_KEY_CONSTRAINT:
                    message.setSummary("Error: Course "+ student.getCourse().getId().toUpperCase() +" does not exist!");
                    break;
                case Codes.DUPLICATE_ENTRY:
                    message.setSummary("Error: "+ e.getMessage());
                    break;
                case Codes.DATA_TRUNCATION:
                    message.setSummary("Error: "+ e.getMessage());
                    break;
                default :
                    System.out.println("Unknown code: "+ e.getErrorCode());
                    e.printStackTrace();
            }

            FacesContext.getCurrentInstance().addMessage(null, message);

            return "";
        }
        catch (SQLException | NullPointerException e) {
			FacesMessage message =
                    new FacesMessage("Error: Cannot connect to MySQL Database");
            FacesContext.getCurrentInstance().addMessage(null, message);
		}

        // if no errors
        try{
            Neo4jDAO.addStudent(student);   // <-- add student to neo4j DB
        }
        catch (ServiceUnavailableException e){
            FacesMessage message =
                    new FacesMessage("Warning: Student "+ student.getName() +" has not been added to Neo4j DB, as it offline." );
            FacesContext.getCurrentInstance().addMessage(null, message);
            return AppPages.STUDENTS;
        }


        return AppPages.STUDENTS;
    }

    /**
     * Delete student from DB's (MySQL & Neo4j)
     * @param student we want to delete.
     * @return page name we go after we done.
     */
    public String deleteStudent(StudentModel student){
        System.out.println("StudentController.deleteStudent(StudentModel student)");
        // Neo4j
        try{
            Neo4jDAO.deleteStudent(student);   // <-- delete student from neo4j DB
        }
        catch (ClientException e){
            FacesMessage message =
                    new FacesMessage("Error: Student: "+ student.getName() +" has not ben deleted from DB as he/she has relationships in Neo4j DB");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return AppPages.STUDENTS;
        }
        catch (ServiceUnavailableException e){
            FacesMessage message =
                    new FacesMessage("Warning: Student "+ student.getName() +" has not been deleted from Neo4j DB, as it offline." );
            FacesContext.getCurrentInstance().addMessage(null, message);
            return AppPages.STUDENTS;
        }
        // MySQL
        try {
            this.dao.deleteStudent(student);
        } 
        catch (SQLException | NullPointerException e) {
			FacesMessage message =
                    new FacesMessage("Error: Cannot connect to MySQL Database");
            FacesContext.getCurrentInstance().addMessage(null, message);
		}

        return AppPages.STUDENTS;
    }

}
