package com.student.Models;

import javax.faces.bean.ManagedBean;
import java.util.MissingFormatArgumentException;
import java.util.Objects;
import java.util.function.Supplier;

@ManagedBean
public class StudentModel {
    private String id;
    private String name;
    private String address;
    private CourseModel course;

    public StudentModel() {
        this.course = new CourseModel();
    }

    public StudentModel(String id, String name, String address, CourseModel course) {
        this.id = id;
        this.name = name;
        this.address = address;

        // deep copy
        String message = "Student is only student if he/she attends course";
        Supplier<MissingFormatArgumentException> noCourseException = () -> new MissingFormatArgumentException(message);
        this.course = CourseModel.of(course).orElseThrow(noCourseException);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CourseModel getCourse() {
        return course;
    }

    public void setCourse(CourseModel course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentModel that = (StudentModel) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
