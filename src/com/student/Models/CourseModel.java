package com.student.Models;

import javax.faces.bean.ManagedBean;
import java.util.Optional;


@ManagedBean
public class CourseModel {
    private String id;
    private String name;
    private int duration;

    public CourseModel(){}

    public CourseModel(String id, String name, int duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }

    /**
     * Deep copy CourseModel
     * @param course we copy from
     * @return deep copy of given object
     */
    public static Optional<CourseModel> of(CourseModel course){
        CourseModel R = new CourseModel();

        try{
            R.setId(course.getId());
            R.setDuration(course.getDuration());
            R.setName(course.getName());
        }
        catch (NullPointerException e){
            return Optional.empty();
        }

        return Optional.of(R);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "name: "+ name +", id: "+ id +", duration: "+ duration;
    }
}

